package sheridan;

public class Celsius {

	public static int fromFahrenheit(int argument) {
		return (int) Math.ceil((argument*1.8)+32);
	}
}
