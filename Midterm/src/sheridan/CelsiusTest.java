package sheridan;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


//@Author: Thomas Chapman 991538703
class CelsiusTest {

	@Test
	void test_fromFahrenheit_regular() {
		assertEquals(Celsius.fromFahrenheit(0), 32);
	}
	
	@Test
	void test_fromFahrenheit_exceptional() {
		assertNotEquals(Celsius.fromFahrenheit(30), 62);
	}
	
	@Test
	void test_fromFahrenheit_boundryIn() {
		assertEquals(Celsius.fromFahrenheit(4), 40);
	}
	
	@Test
	void test_fromFahrenheit_boundryOut() {
		assertNotEquals(Celsius.fromFahrenheit(9), 48);
	}
	
}
